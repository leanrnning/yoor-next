import { FC, useEffect, useState } from "react";
import { getPosts, IMeta, IPost, useGetPosts } from "../api/posts";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import { useRouter } from "next/router";
import { useMutation } from "react-query";
import { LIMIT_HOME } from "../lib/constants";

interface IProps {
    meta?: IMeta;
    setListPost: (post: IPost[]) => void;
    posts?: IPost[];
}

const Paginator: FC<IProps> = ({ meta, setListPost, posts }) => {
    const totalPage = Math.ceil(Number(meta?.total) / LIMIT_HOME);
    const [page, setPage] = useState(1);
    const { mutate, isLoading } = useMutation(getPosts, {
        onSuccess(data, variables, context) {
            setListPost(data.items);
        },
        onError(error, variables, context) {
            console.log(error);
        },
    });
    const handlePaginationChange = (e: any, page: number) => {
        const offset = (page - 1) * LIMIT_HOME;
        mutate({ offset: offset, limit: LIMIT_HOME });
        setPage(page);
    };
    return (
        <Stack spacing={3} className="w-100">
            <Pagination
                count={totalPage}
                className="Pager"
                page={page}
                onChange={handlePaginationChange}
            />
        </Stack>
    );
};

export default Paginator;
