import React from "react";
import Header from "./header";
import Footer from "./footer";

// @ts-ignore
export default function Layout({ children }) {
    return (
        <div className="l-wrapper">
            <Header />
            {children}
            <Footer />
        </div>
    );
}
