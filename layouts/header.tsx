import Link from "next/link";
import { useRouter } from "next/router";

const Header = () => {
    const router = useRouter();
    return (
        <header role="banner" className="l-header">
            <div className="Header">
                <div className="Header__inner">
                    <h1>
                        <Link href={"/"} passHref>
                            <a>
                                <span>yoor公式ブログ</span>
                            </a>
                        </Link>
                    </h1>
                </div>
            </div>
        </header>
    );
};

export default Header;
