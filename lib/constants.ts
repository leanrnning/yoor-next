export const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL || 'https://localhost:3000';
export const API_URL = process.env.NEXT_PUBLIC_API_URL!;
export const LIMIT_HOME = 3;
export const START_OFFSET = 0;
