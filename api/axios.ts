import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

// @ts-ignore
import { API_URL } from '@/lib/constants';
import { AxiosError } from 'axios';

export const request = axios.create({
    baseURL: 'https://jindoku-learn.hacocms.com/api/v1'
});

const handleError = async (error: AxiosError) => {
    const data = error?.response?.data;

    // @ts-ignore
    return Promise.reject(data?.meta || data || error);
};

const handleSuccess = async (res: AxiosResponse) => {
    return res.data;
};

const handleRequest = async (config: AxiosRequestConfig) => {
    const token = 'YqWfkPtmBZQ8XtAGxCW9f4zC';
    const draftToken = 'VXRNYe6A8c7Hm67956V3gEFG';
    if (token) {
        config = {
            ...config,
            headers: {
                Authorization: `Bearer ${token}`,
            },
        };
    }

    return config;
};

request.interceptors.response.use(handleSuccess, handleError);

request.interceptors.request.use(handleRequest, (error) => Promise.reject(error));