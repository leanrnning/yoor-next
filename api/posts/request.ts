import { IPost, IPostByIdParams, IPostParams, IPosts } from "./types";
import { request } from "../axios";

export const getPosts = async (params: IPostParams): Promise<IPosts> => {
    const res: any = await request({
        url: "/post",
        method: "GET",
        params: params,
    });

    return {
        items: res.data,
        meta: res.meta,
    };
};

export const getPost = async (params: IPostByIdParams): Promise<any> => {
    const res: any = await request({
        url: `/post/${params.id}`,
        method: "GET",
        params: params,
    });

    return res
};
