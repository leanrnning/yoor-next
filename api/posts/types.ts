import { PagedMeta } from "../../styles/util.types";

export interface IPostParams {
    search?: string;
    offset?: number;
    limit?: number;
}

export interface IPosts {
    items: IPost[];
    meta: PagedMeta;
}

export interface IPost {
    id?: string;
    title?: string;
    intro?: string;
    content?: string;
    image?: string;
    createdAt?: Date | string;
    footer?: string
}

export interface IPostByIdParams {
    id?: any;
}

export interface IMeta {
    total?: number;
    limit?: number;
    offset?: number;
}

export const LIMIT_POST = 10;
