import { useQuery, UseQueryOptions } from "react-query";
import { IPost, IPostByIdParams, IPostParams, IPosts } from "./types";
import { getPost, getPosts } from "./request";

export const useGetPosts = (
    filterParams?: IPostParams,
    option?: UseQueryOptions<IPosts, Error>
) => {
    return useQuery<IPosts, Error>(
        ["/post", filterParams],
        () => {
            const params: IPostParams = {
                ...filterParams,
            };
            return getPosts(params);
        },
        option
    );
};

export const useGetPost = (
    params: IPostByIdParams,
    option?: UseQueryOptions<IPost, Error>
) => {
    return useQuery<IPost, Error>(
        `/post/${params.id}`,
        () => getPost(params),
        option
    );
};
