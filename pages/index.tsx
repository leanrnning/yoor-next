import type { NextPage } from "next";
import Head from "next/head";
import ArticlePost from "../modules/posts/article";
import Paginator from "../layouts/pagination";
import { IMeta, IPost, IPostParams, IPosts, useGetPosts } from "../api/posts";
import React, { useEffect, useState } from "react";
import SearchPost from "../modules/posts/search";
import { LIMIT_HOME, START_OFFSET } from "../lib/constants";

const Home: NextPage = () => {
    const { data } = useGetPosts({offset: START_OFFSET, limit: LIMIT_HOME});
    const [posts, listPost] = useState<IPost[] | undefined>([]);
    const [search, setSearch] = useState<string | undefined>("");
    const [meta, setMeta] = useState<IMeta | undefined>({});

    const getParamSearch = (postParamSearch: IPostParams) => {
        setSearch(postParamSearch.search);
    };
    useEffect(() => {
        listPost(data?.items);
        setMeta(data?.meta);
    }, [data]);
    return (
        <div className="l-content">
            <div className="Main">
                {Number(posts?.length) > 0 ? (
                    <React.Fragment>
                        <div className="Main__contents">
                            {posts?.map((post) => (
                                <ArticlePost key={post.id} post={post} />
                            ))}
                        </div>
                        <Paginator
                            meta={meta}
                            setListPost={(post: IPost[]) => listPost(post)}
                            posts={posts}
                        />
                    </React.Fragment>
                ) : (
                    <p className="ResultHeading">
                        <span>&#34;{search}&#34;</span>がありませんでした
                    </p>
                )}
            </div>
            <SearchPost
                setListPost={(post: IPost[]) => listPost(post)}
                setSearch={getParamSearch}
            />
        </div>
    );
};

export default Home;
