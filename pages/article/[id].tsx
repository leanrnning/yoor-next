import type { NextPage } from "next";
import { useRouter } from "next/router";
import Moment from "react-moment";
import SearchPost from "../../modules/posts/search";
import { IMeta, IPost, IPostParams, IPosts, useGetPost } from "../../api/posts";
import ReactHtmlParser from "react-html-parser";
import { useEffect, useState } from "react";

const ArticleDetailPage: NextPage = () => {
    const router = useRouter();
    const { id } = router.query;
    const post = useGetPost({ id });
    const getParamSearch = (postParamSearch: IPostParams) => {};
    const [posts, listPost] = useState<IPost[] | undefined>([]);
    return (
        <div className="l-content">
            <div className="Main">
                <div className="Main__contents">
                    <article className="Article">
                        <header className="Article__heading">
                            <p className="date">
                                <span>
                                    <Moment format="YYYY年M月D日">
                                        {post?.data?.createdAt}
                                    </Moment>
                                </span>
                            </p>
                            <h2 className="title">
                                <span>{post?.data?.title}</span>
                            </h2>
                        </header>
                        <div className="Article__content">
                            {ReactHtmlParser(post?.data?.content || "")}
                        </div>
                        <footer className="Article__footer">
                            {ReactHtmlParser(post?.data?.footer || "")}
                        </footer>
                    </article>
                </div>
            </div>
            <SearchPost
                setListPost={(post: IPost[]) => listPost(post)}
                setSearch={getParamSearch}
            />
        </div>
    );
};

export default ArticleDetailPage;
