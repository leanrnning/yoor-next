import React, { Dispatch, FC, useEffect, useState } from "react";
import { useMutation } from "react-query";
import { getPosts, IPost, IPostParams, useGetPosts } from "../../api/posts";

interface IProps {
    setListPost: (post: IPost[]) => void;
    setSearch: (search: IPostParams) => void;
}
const SearchPost: FC<IProps> = ({ setListPost, setSearch }) => {
    const [keyword, setKeyword] = useState("");
    const { mutate, isLoading } = useMutation(getPosts, {
        onSuccess(data, variables, context) {
            setListPost(data.items);
        },
        onError(error, variables, context) {
            console.log(error);
        },
    });
    const handleSearch = () => {
        setSearch({ search: keyword });
        mutate({ search: keyword });
    };

    return (
        <aside role="complementary" className="Side">
            <section className="Module -search">
                <h3 className="Module__heading -none">検索</h3>
                <div className="Module__body">
                    <form className="Search">
                        <input
                            name="keyword"
                            type="text"
                            placeholder="ブログ内検索"
                            className="Search__text"
                            onChange={(e) => {
                                setKeyword(e.target.value);
                            }}
                        ></input>
                        <input
                            type="button"
                            value="検索"
                            className="Search__submit"
                            onClick={() => handleSearch()}
                        ></input>
                    </form>
                </div>
            </section>
        </aside>
    );
};

export default SearchPost;
