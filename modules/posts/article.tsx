import Link from "next/link";
import Moment from "react-moment";

const ArticlePost = (props: { post: any }) => {
    const post = props.post;
    return (
        <div>
            <article className="Entry">
                <header className="Entry__heading">
                    <h2 className="title">
                        <Link href={`article/${post.id}`} passHref>
                            <a>
                                <span>{post.title}</span>
                            </a>
                        </Link>
                    </h2>
                </header>
                <p className="Entry__date">
                    <time dateTime={post.createdAt}>
                        <span>
                            <Moment format="YYYY年M月D日">
                                {post.createdAt}
                            </Moment>
                        </span>
                    </time>
                </p>
                <div className="EntryThumbnail">
                    <div className="EntryThumbnail__inner">
                        <Link href={`article/${post.id}`} passHref>
                            <img src={post.image} alt=""></img>
                        </Link>
                    </div>
                </div>
                <div className="Entry__text">{post.intro}</div>
                <p className="Entry__readMore">
                    <Link href={`article/${post.id}`} passHref>
                        <a className="ReadMore -en">
                            <span>read more</span>
                        </a>
                    </Link>
                </p>
            </article>
        </div>
    );
};

export default ArticlePost;
