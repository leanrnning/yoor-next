export type PagedMeta = {
    limit: number;
    offset: number;
    total: number;
};
